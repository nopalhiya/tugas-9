<?php

class Frog{
    public $name;

    function __construct($nama, $leg = 4, $cold = "no"){
        $this->name = $nama;
        $this->legs = $leg;
        $this->cold_blooded = $cold;
    }
    public function jump(){
        return "hop hop\n";
    }
}
?>
