<?php
require_once("animal.php");
require_once("Ape.php");
require_once("Frog.php");

$sheep = new Animal("shaun");

echo "Name: ".$sheep->name."\n"; // "shaun"
echo "legs: ".$sheep->legs."\n"; // 4
echo "cold blooded: ".$sheep->cold_blooded."\n"; // "no"

// NB: Boleh juga menggunakan method get (get_name(), get_legs(), get_cold_blooded())

echo "\n";
// index.php
$sungokong = new Ape("kera sakti");
echo "Name: ".$sungokong->name."\n"; // "shaun"
echo "legs: ".$sungokong->legs."\n"; // 4
echo "cold blooded: ".$sungokong->cold_blooded."\n"; // "no"
echo "Yell: ".$sungokong->yell(); // "Auooo"

echo "\n";

$kodok = new Frog("buduk");
echo "Name: ".$kodok->name."\n"; // "shaun"
echo "legs: ".$kodok->legs."\n"; // 4
echo "cold blooded: ".$kodok->cold_blooded."\n"; // "no"
echo "Jump: ".$kodok->jump(); // "hop hop"
 



?>