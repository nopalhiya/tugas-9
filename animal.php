<?php
class Animal{

    function __construct($nama, $leg = 4, $cold = "no"){
        $this->name = $nama;
        $this->legs = $leg;
        $this->cold_blooded = $cold;
    }
}
