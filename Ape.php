<?php

class Ape{
    public $name;
    
    function __construct($nama, $leg = 2, $cold = "no"){
        $this->name = $nama;
        $this->legs = $leg;
        $this->cold_blooded = $cold;
    }
    
    public function yell(){
        return "Auooo\n";
    }
}
?>
